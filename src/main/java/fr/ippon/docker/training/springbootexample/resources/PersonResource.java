package fr.ippon.docker.training.springbootexample.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.ippon.docker.training.springbootexample.domain.Person;
import fr.ippon.docker.training.springbootexample.repository.PersonRepository;

/**
 * PersonResource
 */
@RestController
@RequestMapping("/api/person")
public class PersonResource {
    @Autowired
    private PersonRepository personRepository;

    @GetMapping("")
    public List<Person> getPerson()
    {
        return personRepository.findAll();
    }
    
}